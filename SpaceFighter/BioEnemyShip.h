
#pragma once

#include "EnemyShip.h"

class BioEnemyShip : public EnemyShip
{
	Vector2 pp;

public:

	BioEnemyShip();
	virtual ~BioEnemyShip() { }

	void SetTexture(Texture *pTexture) { m_pTexture = pTexture; }

	virtual void Update(const GameTime *pGameTime);

	virtual void Draw(SpriteBatch *pSpriteBatch);

	void SetPP(Vector2 POS) { pp = POS; }

private:

	Texture *m_pTexture;

};