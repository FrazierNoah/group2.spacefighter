
#include "BioEnemyShip.h"
#include "Level01.h"

BioEnemyShip::BioEnemyShip()
{
	SetSpeed(150);
	SetMaxHitPoints(1);
	SetCollisionRadius(20);
}

void BioEnemyShip::Update(const GameTime *pGameTime)
{
	if (IsActive())
	{
		BioEnemyShip::SetPP(GetCurrentLevel()->GetPlayerShip());
		float x = sin(pGameTime->GetTotalTime() * Math::PI + GetIndex());
		x *= GetSpeed() * pGameTime->GetTimeElapsed() * 1.4f;
		if (GetPosition().Y < (40 + pp.Y) && GetPosition().Y > (-40 + pp.Y)) {
			if (pp.X > BioEnemyShip::GetPosition().X) { 
				TranslatePosition(50, (BioEnemyShip::GetSpeed() * pGameTime->GetTimeElapsed()));
			}
			else if (pp.X < BioEnemyShip::GetPosition().X) { 
				TranslatePosition(-50, (BioEnemyShip::GetSpeed() * pGameTime->GetTimeElapsed()));
			}
			/*x = pp.X - BioEnemyShip::GetPosition().X;
			std::cout << pp.X << " GetPosition(): " << BioEnemyShip::GetPosition().X <<"\n";
			TranslatePosition(x, (GetSpeed() * pGameTime->GetTimeElapsed())*3);*/
		}
		else {
			TranslatePosition(x, GetSpeed() * pGameTime->GetTimeElapsed());
		}

		if (!IsOnScreen()) Deactivate();
	}

	EnemyShip::Update(pGameTime);
}


void BioEnemyShip::Draw(SpriteBatch *pSpriteBatch)
{
	if (IsActive())
	{
		pSpriteBatch->Draw(m_pTexture, GetPosition(), Color::White, m_pTexture->GetCenter(), Vector2::ONE, Math::PI, 1);
	}
}
